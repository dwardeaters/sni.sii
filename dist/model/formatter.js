jQuery.sap.declare("SNI.SII.model.formatter");

SNI.SII.model.formatter = {

	_jsonParse: function(oJson) {
		var t = this;
		return JSON.parse(oJson);
	},

	numberUnit: function(sValue) {
		if (!sValue) {
			return "";
		}
		return parseFloat(sValue).toFixed(2);
	},
	formateDate: function(value) {  
		// return value;
		if (!value) {
			return value;
		}

		var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
			pattern: "dd.MM.yyyy"
		});
		//  var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "yyyyMMdd" });
		var dateFormatted = dateFormat.format(value);
		return dateFormatted;
	},

	formatTime: function(value) {
		var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
			pattern: "HHmmss"
		});
		var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
		var timeStr = timeFormat.format(value);

		return timeStr;
	},

	timeFormat: function(time) {
		// return time;
		if (time == undefined) return "";
		var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
			pattern: "HH:mm"
		});
		var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
		var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs));
		return timeStr;
	},

	formateDate2: function(value) {
		if (value) {
			var items = value.split("/Date(");
			if (items) {
				var date = items[1].split(")/")[0];
				return new Date(parseInt(date));
			}
		}
		return "";
	},

	formatIconColor: function(status) {
		if (status === "A") {
			return "green";
		} else if (status === "R") {
			return "red";
		}
	},
	formatIcon: function(status) {
		if (status === "A") {
			return 'sap-icon://accept';
		} else if (status === "R") {
			return 'sap-icon://decline';
		}
	},

	enabledModel: function(oChange, oCreate) {   
	var model = {
			"Zsftn"             :   this.getEnableMod(null,oChange,oCreate),
			"Erzet"             :   this.getEnableMod(null,oChange,oCreate),
			"Aezet"             :   this.getEnableMod(null,oChange,oCreate),
			"Zsfttyp"           :   this.getEnableMod(null,oChange,oCreate),
			"Apprst"            :   this.getEnableMod(null,oChange,oCreate),
			"Apprs"             :   this.getEnableMod(null,oChange,oCreate),
			"Zsftat"            :   this.getEnableMod(null,oChange,oCreate),
			"Bukrs"             :   this.getEnableMod(null,oChange,oCreate),
			"Lifnr"             :   this.getEnableMod(null,oChange,oCreate),
			"Kunnr"             :   this.getEnableMod(null,oChange,oCreate),
			"Objkey"            :   this.getEnableMod(null,oChange,oCreate),
			"Object"            :   this.getEnableMod(null,oChange,oCreate),
			"Instance"          :   this.getEnableMod(null,oChange,oCreate),
			"Idversion"         :   this.getEnableMod(null,oChange,oCreate),
			"Appl"              :   this.getEnableMod(null,oChange,oCreate),
			"Event"             :   this.getEnableMod(null,oChange,oCreate),
			"Fyon"              :   this.getEnableMod(null,oChange,oCreate),
			"Fkart"             :   this.getEnableMod(null,oChange,oCreate),
			"Xreversal"         :   this.getEnableMod(null,oChange,oCreate),
			"Ilgili"            :   this.getEnableMod(null,oChange,oCreate),
			"Period"            :   this.getEnableMod(null,oChange,oCreate),
			"Supcodigo"         :   this.getEnableMod(null,oChange,oCreate),
			"Suptaxidtype"      :   this.getEnableMod(null,oChange,oCreate),
			"Suptaxnr"          :   this.getEnableMod(null,oChange,oCreate),
			"Supname1"          :   this.getEnableMod(null,oChange,oCreate),
			"Invserial"         :   this.getEnableMod(null,oChange,oCreate),
			"InvserialTo"       :   this.getEnableMod(null,oChange,oCreate),
			"InvType"           :   this.getEnableMod(null,oChange,oCreate),
			"CorrType"          :   this.getEnableMod(null,oChange,oCreate),
			"InvScheme"         :   this.getEnableMod(null,oChange,oCreate),
			"InvSchemei"        :   this.getEnableMod(null,oChange,oCreate),
			"InvScheme1"        :   this.getEnableMod(null,oChange,oCreate),
			"InvScheme2"        :   this.getEnableMod(null,oChange,oCreate),
			"Bktxt"             :   this.getEnableMod(null,oChange,oCreate),
			"Rectaxnr"          :   this.getEnableMod(null,oChange,oCreate),
			"Rectaxidtype"      :   this.getEnableMod(null,oChange,oCreate),
			"Reccodigo"         :   this.getEnableMod(null,oChange,oCreate),
			"Recname1"          :   this.getEnableMod(null,oChange,oCreate),
			"Isthirdp"          :   this.getEnableMod(null,oChange,oCreate),
			"Ernam"             :   this.getEnableMod(null,oChange,oCreate),
			"Aenam"             :   this.getEnableMod(null,oChange,oCreate),
			"Aciklama"          :   this.getEnableMod(null,oChange,oCreate),
			"FiscYear"          :   this.getEnableMod(null,oChange,oCreate),
			"Blgpbr"            :   this.getEnableMod(null,oChange,oCreate),
			"GauthId"           :   this.getEnableMod(null,oChange,oCreate),
			"EkAlan1"           :   this.getEnableMod(null,oChange,oCreate),
			"EkAlan2"           :   this.getEnableMod(null,oChange,oCreate),
			"Resptype"          :   this.getEnableMod(null,oChange,oCreate),
			"Response"          :   this.getEnableMod(null,oChange,oCreate),
			"Pymethod"          :   this.getEnableMod(null,oChange,oCreate),
			"Iban"              :   this.getEnableMod(null,oChange,oCreate),
			"RectInvserial"     :   this.getEnableMod(null,oChange,oCreate),
			"Propstat"          :   this.getEnableMod(null,oChange,oCreate),
			"Propregist"        :   this.getEnableMod(null,oChange,oCreate),
			"IntraType"         :   this.getEnableMod(null,oChange,oCreate),
			"IntraFilled"       :   this.getEnableMod(null,oChange,oCreate),
			"IntraMembst"       :   this.getEnableMod(null,oChange,oCreate),
			"IntraOp"           :   this.getEnableMod(null,oChange,oCreate),
			"CapInvid"          :   this.getEnableMod(null,oChange,oCreate),
			"Vbtyp"             :   this.getEnableMod(null,oChange,oCreate),
			"Blart"             :   this.getEnableMod(null,oChange,oCreate),
			"Vorgang"           :   this.getEnableMod(null,oChange,oCreate),
			"Zarfid"            :   this.getEnableMod(null,oChange,oCreate),
			"Zarfpos"           :   this.getEnableMod(null,oChange,oCreate),
			"Sfakn"             :   this.getEnableMod(null,oChange,oCreate),
			"Rsftn"             :   this.getEnableMod(null,oChange,oCreate),
			"Iftyp"             :   this.getEnableMod(null,oChange,oCreate),
			"ItDoctyp"          :   this.getEnableMod(null,oChange,oCreate),
			"Mwskzheader"       :   this.getEnableMod(null,oChange,oCreate),
			"ItRectaxnr"        :   this.getEnableMod(null,oChange,oCreate),
			"ItSuptaxnr"        :   this.getEnableMod(null,oChange,oCreate),
			"Fidocnr"           :   this.getEnableMod(null,oChange,oCreate),
			"Fifiscal"          :   this.getEnableMod(null,oChange,oCreate),
			"HuDoctyp"          :   this.getEnableMod(null,oChange,oCreate),
			"Hash"              :   this.getEnableMod(null,oChange,oCreate),
			"Selfbind"          :   this.getEnableMod(null,oChange,oCreate),
			"HuPymmethod"       :   this.getEnableMod(null,oChange,oCreate),
			"HuInvsource"       :   this.getEnableMod(null,oChange,oCreate),
			"HuCounty"          :   this.getEnableMod(null,oChange,oCreate),
			"HuAnnulment"       :   this.getEnableMod(null,oChange,oCreate),
			"InvArt7"           :   this.getEnableMod(null,oChange,oCreate),
			"InvArt6"           :   this.getEnableMod(null,oChange,oCreate),
			"Suctaxnr"          :   this.getEnableMod(null,oChange,oCreate),
			"Sucname1"          :   this.getEnableMod(null,oChange,oCreate),
			"RegPrevio"         :   this.getEnableMod(null,oChange,oCreate),
			"OrgGas"            :   this.getEnableMod(null,oChange,oCreate),
			"AddCadRef"         :   this.getEnableMod(null,oChange,oCreate),
			"Xblnr"             :   this.getEnableMod(null,oChange,oCreate),
			"Uuid"              :   this.getEnableMod(null,oChange,oCreate),
			"Supdestcode"       :   this.getEnableMod(null,oChange,oCreate),
			"Recdestcode"       :   this.getEnableMod(null,oChange,oCreate),
			"PaDoctyp"          :   this.getEnableMod(null,oChange,oCreate),
			"ExtUuid"           :   this.getEnableMod(null,oChange,oCreate),
			"Partnty"           :   this.getEnableMod(null,oChange,oCreate),
			"GrossAmnt"         :   this.getEnableMod(null,oChange,oCreate),
			"CuotDed"           :   this.getEnableMod(null,oChange,oCreate),
			"RectBase"          :   this.getEnableMod(null,oChange,oCreate),
			"RectQuota"         :   this.getEnableMod(null,oChange,oCreate),
			"CapUperc"          :   this.getEnableMod(null,oChange,oCreate),
			"Kurrf"             :   this.getEnableMod(null,oChange,oCreate),
			"TransreAmnt"       :   this.getEnableMod(null,oChange,oCreate),
			"TotTaxAmnt"        :   this.getEnableMod(null,oChange,oCreate),
			"Erdat"             :   this.getEnableMod(null,oChange,oCreate),
			"Aedat"             :   this.getEnableMod(null,oChange,oCreate),
			"Fkdat"             :   this.getEnableMod(null,oChange,oCreate),
			"Bldat"             :   this.getEnableMod(null,oChange,oCreate),
			"Budat"             :   this.getEnableMod(null,oChange,oCreate),
			"RectFkdat"         :   this.getEnableMod(null,oChange,oCreate),
			"CapInvdate"        :   this.getEnableMod(null,oChange,oCreate),
			"BldatCust"         :   this.getEnableMod(null,oChange,oCreate),
			"HuPymdate"         :   this.getEnableMod(null,oChange,oCreate),
			"WadatIst"          :   this.getEnableMod(null,oChange,oCreate),
			"DeliveryStart"     :   this.getEnableMod(null,oChange,oCreate),
			"DeliveryEnd"       :   this.getEnableMod(null,oChange,oCreate),
			"Archivestat"       :   this.getEnableMod(null,oChange,oCreate),
			"Reconcilied"       :   this.getEnableMod(null,oChange,oCreate),
			"Informed"          :   this.getEnableMod(null,oChange,oCreate),
			"CashInd"           :   this.getEnableMod(null,oChange,oCreate),
			"HuInterm"          :   this.getEnableMod(null,oChange,oCreate)
		};
		return model;

	},
	
	getEnableMod: function(display,change,create){
		if(display||change||create){
			return true;
		}else{
			return false;
		}
	}

};