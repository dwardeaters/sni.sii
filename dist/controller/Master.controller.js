/*global location history */
sap.ui.define([
	"SNI/SII/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"SNI/SII/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/m/Link",
	"sap/m/MessagePopover",
	"sap/m/MessagePopoverItem",
	"sap/ui/core/mvc/XMLView",
	"SNI/SII/model/util"
], function(BaseController, JSONModel, formatter, Filter, FilterOperator, MessageToast, MessageBox, Link, MessagePopover, MessageItem,
	XMLView, util) {
	"use strict";

	return BaseController.extend("SNI.SII.controller.Master", {

		formatter: formatter,

		onInit: function() {
			this.busyCount = 0;
			var oTable;
			oTable = this.getView().byId("smartTable_ResponsiveTable").getTable();
			oTable.attachRowSelectionChange(this.onTableSelection, this);
			// var oViewModel,
			// 	iOriginalBusyDelay,
			// 	oTable = this.byId("table");

			// Put down worklist table's original value for busy indicator delay,
			// so it can be restored later on. Busy handling on the table is
			// taken care of by the table itself.
			// iOriginalBusyDelay = oTable.getBusyIndicatorDelay();
			// // keeps the search state
			// this._aTableSearchState = [];

			// // Model used to manipulate control states
			// oViewModel = new JSONModel({
			// 	worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
			// 	shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
			// 	shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
			// 	shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
			// 	tableNoDataText : this.getResourceBundle().getText("tableNoDataText"),
			// 	tableBusyDelay : 0
			// });
			// this.setModel(oViewModel, "worklistView");

			// // Make sure, busy indication is showing immediately so there is no
			// // break after the busy indication for loading the view's meta data is
			// // ended (see promise 'oWhenMetadataIsLoaded' in AppController)
			// oTable.attachEventOnce("updateFinished", function(){
			// 	// Restore original busy indicator delay for worklist's table
			// 	oViewModel.setProperty("/tableBusyDelay", iOriginalBusyDelay);
			// });

		},

		onTableSelection: function() {
			var aSelectedItems = this.getView().byId("smartTable_ResponsiveTable").getTable().getSelectedIndices();
			this.getView().byId("idApprove").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idArchive").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idCanceled").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idCheck").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idDisplay").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idEmailSend").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idLogDetail").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idCreateDocument").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idChangeDocument").setEnabled(aSelectedItems.length > 0);
			this.getView().byId("idDisplayDocument").setEnabled(aSelectedItems.length > 0);
		},

		onDataReceived: function(oEvent) {
			/*	this.getVariant(); */
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Triggered by the table's 'updateFinished' event: after new table
		 * data is available, this handler method updates the table counter.
		 * This should only happen if the update was successful, which is
		 * why this handler is attached to 'updateFinished' and not to the
		 * table's list binding's 'dataReceived' method.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the worklist's object counter after the table update
			// var sTitle,
			// 	oTable = oEvent.getSource(),
			// 	iTotalItems = oEvent.getParameter("total");
			// // only update the counter if the length is final and
			// // the table is not empty
			// if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
			// 	sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			// } else {
			// 	sTitle = this.getResourceBundle().getText("worklistTableTitle");
			// }
			// this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},

		/**
		 * Event handler when a table item gets pressed
		 * @param {sap.ui.base.Event} oEvent the table selectionChange event
		 * @public
		 */
		onPress: function(oEvent) {
			// The source is the list item that got pressed
			this._showObject(oEvent.getSource());
		},

		/**
		 * Event handler for navigating back.
		 * We navigate back in the browser historz
		 * @public
		 */
		onNavBack: function() {
			history.go(-1);
		},

		onSearch: function(oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
			} else {
				var aTableSearchState = [];
				var sQuery = oEvent.getParameter("query");

				if (sQuery && sQuery.length > 0) {
					aTableSearchState = [new Filter("Matnr", FilterOperator.Contains, sQuery)];
				}
				this._applySearch(aTableSearchState);
			}

		},

		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function() {
			var oTable = this.byId("table");
			oTable.getBinding("items").refresh();
		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Shows the selected item on the object page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showObject: function(oItem) {
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Zsftn")
			});
		},

		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
		 * @private
		 */
		_applySearch: function(aTableSearchState) {
			var oTable = this.byId("table"),
				oViewModel = this.getModel("worklistView");
			oTable.getBinding("items").filter(aTableSearchState, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aTableSearchState.length !== 0) {
				oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
			}
		},
		_xmlParse: function(oXml) {
			var t = this;
			var parser = new DOMParser();
			return parser.parseFromString(oXml, "text/xml");
		},

		_jsonParse: function(oJson) {
			var t = this;
			return JSON.parse(oJson);
		},

		_showBusy: function() {
			this.busyCount += 1;
			sap.ui.core.BusyIndicator.show();
		},

		_hideBusy: function() {
			this.busyCount -= 1;
			if (this.busyCount <= 0) {
				this.busyCount = 0;
				sap.ui.core.BusyIndicator.hide();
			}
		},

		onApprove: function() {
			var t = this;
			this._showBusy();

			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			var sObjectPath = this.oView.getModel().createKey("/ApproveSet", {
				Zsftn: line.Zsftn
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		onArchive: function() {
			var t = this;
			this._showBusy();
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			var sObjectPath = this.oView.getModel().createKey("/ArchiveSet", {
				Zsftn: line.Zsftn
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		onCancaled: function() {
			var t = this;
			this._showBusy();
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}

			var sObjectPath = this.oView.getModel().createKey("/CanceledSet", {
				Zsftn: line.Zsftn
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		onCheck: function() {
			var t = this;
			this._showBusy();
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			var sObjectPath = this.oView.getModel().createKey("/CheckSet", {
				Zsftn: line.Zsftn
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		onEmailSend: function() {
			var t = this;
			this._showBusy();
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			var sObjectPath = this.oView.getModel().createKey("/EmailSendSet", {
				Zsftn: line.Zsftn
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		onLogDetail: function() {
			var t = this;
			this._showBusy();
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			var sObjectPath = this.oView.getModel().createKey("/DisplaySet", {
				Zsftn: line.Zsftn,
				Type: 'HTML'
			});
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},

		// 
		onDisplay: function(oEvent) {
			var t = this;
			this._showBusy();
			this.sfilter = this.byId("idInvoiceDialog");

			this.invoiceFragment = "idInvoiceFragment";
			var fragmentId = this.getView().createId(this.invoiceFragment);
			var idPanel = sap.ui.core.Fragment.byId(fragmentId, "idPanel");
			this.idPnl = idPanel;

			var line = this.getSelectedLine();
			if (!line) {
				return;
			}

			var sObjectPath = this.oView.getModel().createKey("/DisplaySet", {
				Zsftn: line.Zsftn, //"000000005",
				Type: 'HTML' //"2019"
			});

			// return;
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					var html = new sap.ui.core.HTML();
					html.setContent(t.b64DecodeUnicode(oData.Content));
					t.idPnl.destroyContent();
					t.idPnl.addContent(html);
					t.sfilter.open();
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);
					t._hideBusy();
				},
				error: function(err) {
					sap.ui.core.Fragment.byId(fragmentId, "idNotifLog").setBusy(false);
					t._hideBusy();
				}
			});

			// var html = new sap.ui.core.HTML();
			// html.setContent("<iframe src=\"http://www.karadere.com\" style=\"height:600px;width:600px;\"></iframe>");
			// idPanel.addContent(html);
			// this.sfilter.open();

			// var SozDialog = new sap.ui.commons.Dialog({modal: true});
			// SozDialog.setTitle("Popup title");
			// SozDialog.setWidth("650px");
			// SozDialog.setHeight("650px");

			// SozDialog.addContent(html);
			// SozDialog.addButton(new sap.ui.commons.Button({text: "KAPAT", press:function(){SozDialog.close();}}));
			// SozDialog.open();
		},

		getSelectedLine: function() {
			var oTable = this.byId("smartTable_ResponsiveTable").getTable();
			var row = oTable.getSelectedIndex();
			var selectedRow = oTable.getRows()[row];
			if (selectedRow) {
				var sPath = selectedRow.getBindingContext().sPath;
				return selectedRow.getModel().getObject(sPath);
			} else {
				MessageToast.show("Lütfen satır seçiniz");
				return null;
			}
		},

		b64DecodeUnicode: function(str) {
			// Going backwards: from bytestream, to percent-encoding, to original string.
			return decodeURIComponent(atob(str).split('').map(function(c) {
				return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
			}).join(''));
		},

		handleDisplayCancel: function() {
			this.sfilter.close();
		},

		handleXMLDisplay: function() {
			var t = this;
			this.invoiceXML = this.byId("idInvoiceXMLDialog");
			this.invoiceXMLFragment = "idInvoiceXMLFragment";
			var fragmentId = this.getView().createId(this.invoiceXMLFragment);
			this.idPanelXML = sap.ui.core.Fragment.byId(fragmentId, "idPanelXML");
			/*	var oXml = "<note><to>Tove</to><from>Jani</from><heading>Reminder</heading><body>Don't forget me this weekend!</body></note>";
				var parser = new DOMParser();
				var b = parser.parseFromString(oXml, "text/xml");*/

			/*	idPanelXML.destroyContent();
				idPanelXML.addContent(b);*/
			var sObjectPath = this.oView.getModel().createKey("/DisplaySet", {
				Zsftn: "005000000", //"005000000",
				Type: 'XML' //"2019"
			});

			// return;
			this.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {
					debugger;
					var xmlparser = new DOMParser();
					var xmldoc = xmlparser.parseFromString(atob(oData.Content), "text/xml");
					t.idPanelXML.setDOMContent(xmldoc);
					/*		var viewHtml = new sap.ui.core.HTML().setDOMContent(atob(oData.Content));
							t.idPanelXML.placeAt('viewHtml');*/
					/*					var html = new sap.ui.core.HTML();
										html.setDOMContent(atob(oData.Content));
										t.idPanelXML.destroyContent();
										t.idPanelXML.addContent(html);*/
					var oMessage = t._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);
					t._hideBusy();
				},
				error: function(err) {
					sap.ui.core.Fragment.byId(fragmentId, "idNotifLog").setBusy(false);
					t._hideBusy();
				}
			});

			this.invoiceXML.open();
		},

		handleHTMLDisplay: function() {
			var t = this;

			this.sfilter = this.byId("idInvoiceDialog");
			this.sfilter.open();
		},

		handleMessagePopoverPress: function(oEvent) {
			if (!this.oMessagePopover) {
				this.createMessagePopover();
			}
			this.oMessagePopover.toggle(oEvent.getSource());
		},

		createMessagePopover: function(oEvent) {
			var that = this;
			var oLink = new Link({
				text: "Show more information",
				href: "http://google.com",
				target: "_blank"
			});

			var oMessageTemplate = new MessageItem({
				type: '{type}',
				title: '{title}',
				activeTitle: "{active}",
				description: '{description}',
				subtitle: '{subtitle}',
				counter: '{counter}',
				link: oLink
			});

			this.oMessagePopover = new MessagePopover({
				items: {
					path: '/',
					template: oMessageTemplate
				},
				activeTitlePress: function() {
					MessageToast.show('Active title is pressed');
				}
			});
		},

		setPopupMessage: function(data, oStatu) {
			var t = this;
			var sOdata = [];
			var sNew = [];
			if (!this.oMessagePopover) {
				this.createMessagePopover();
			}

			sNew = this.addMessage(data);
			var oModel = new JSONModel();
			oModel.setData(sNew);
			this.byId("idPopup").setText(sNew.length);
			this.oMessagePopover.setModel(oModel);
			if (!oStatu) {
				this.byId("idPopup").addDependent(this.oMessagePopover);
				this.oMessagePopover.openBy(this.byId("idPopup"));
			}
		},

		addMessage: function(line) {
			var t = this;
			var returnMessage = [];
			if (line.detail) {
				for (var i = 0; i < line.details.length; i++) {
					var sMessage = {
						type: line.details[i].code.split("/")[0],
						title: line.details[i].code.split("/")[0] + " Message",
						description: line.details[i].message,
						subtitle: line.details[i].code.split("/")[1],
						counter: 1
					};
					returnMessage.push(sMessage);
				}

			}
			var sMessage = {
				type: line.severity == "success" ? "Success" : "Error",
				title: line.code.split("/")[0] + " Message",
				description: line.message,
				subtitle: line.code.split("/")[1],
				counter: 1
			};
			returnMessage.push(sMessage);
			return returnMessage;
		},
		onDisplayDocument: function(oItem) {

			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			this.getRouter().navTo("displayDocument", {
				Zsftn: line.Zsftn,
				oType: "display"
			});
		},
		onChangeDocument: function() {
			var t = this;
			var line = this.getSelectedLine();
			if (!line) {
				return;
			}
			this.getRouter().navTo("displayDocument", {
				Zsftn: line.Zsftn,
				oType: "change"
			});
		},

		onCreateDocument: function() {
			var t = this;
	 
			this.getRouter().navTo("displayDocument", {
				Zsftn: "NEW",
				oType: "create"
			});
		}

	});
});