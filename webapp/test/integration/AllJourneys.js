/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"test/test/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"test/test/test/integration/pages/Worklist",
	"test/test/test/integration/pages/Object",
	"test/test/test/integration/pages/NotFound",
	"test/test/test/integration/pages/Browser",
	"test/test/test/integration/pages/App"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "test.test.view."
	});

	sap.ui.require([
		"test/test/test/integration/WorklistJourney",
		"test/test/test/integration/ObjectJourney",
		"test/test/test/integration/NavigationJourney",
		"test/test/test/integration/NotFoundJourney"
	], function () {
		QUnit.start();
	});
});