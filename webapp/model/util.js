sap.ui.define([], function() {
	"use strict";

	return {

		onNavBack: function(backPageCount, isLaunchpad) {
			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sap.ushell) {
				var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
				if (isLaunchpad == undefined) {
					history.go(backPageCount);
				} else {
					//history.go(-1);
					oCrossAppNavigator.toExternal({
						target: {
							shellHash: "#"
						}
					});
				}
			} else {
				history.go(backPageCount);
			}
		}

	};

});