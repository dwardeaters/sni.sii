sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"SNI/SII/model/formatter",
	"sap/ui/model/json/JSONModel",
	"sap/m/Link",
	"sap/m/MessagePopover",
	"sap/m/MessagePopoverItem",
	'sap/ui/model/Filter'
], function(Controller, formatter, JSONModel, Link, MessagePopover, MessageItem, Filter) {
	"use strict";

	return Controller.extend("SNI.SII.controller.DisplayDocument", {
		formatter: formatter,
		onInit: function() {
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sni/sii_doc_fio_srv", {});
			this.getView().setModel(oModel);
			this.oView = this.getView();
			this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
			this.oRouter = this.oComponent.getRouter();
			this.oRouter.getRoute("displayDocument").attachMatched(this.onRoutePatternMatched, this);

		},
		onRoutePatternMatched: function(oEvent) {
			this.busyCount = 0;
			this.sPageName = oEvent.getParameter("name");
			this.oZsftn = oEvent.getParameter("arguments").Zsftn;
			this.oType = oEvent.getParameter("arguments").oType;
			this.onEnabledMode(this.oZsftn, this.oType);
		},

		test: function(that, oZsftn) {
			var t = that;
			var sObjectPath = t.oView.getModel().createKey("/DocumentSet", {
				Zsftn: oZsftn
			});

			t.oView.getModel().read(sObjectPath, {
				success: function(oData, oResponse) {

					var oModel = new JSONModel(oData);

					t.oView.setModel(oModel, "DisplayDocumentModel");

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
			
			var sObjectPath = t.oView.getModel().createKey("/DocumentSet", {
				Zsftn: oZsftn
			});

			t.oView.getModel().read(sObjectPath+"/ItemSet", {
				success: function(oData, oResponse) {
					
					// oData.Erzet = t.convTime_To_Date(oData.Erzet);
					// oData.Aezet = t.convTime_To_Date(oData.Aezet);
					
					var oModel = new JSONModel({
						"ItemSet":oData.results
						});

					t.oView.setModel(oModel, "DetailItemModel"); 

					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},

		readData: function(oZsftn) {
			var t = this;

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sni/sii_doc_fio_srv", {
				loadMetadataAsync: false
			});
			oModel.attachMetadataLoaded(function() {
				t.test(t, oZsftn);
			});

		},

		onNavBack: function() {
			history.go(-1);
		},
		_showBusy: function() {
			this.busyCount += 1;
			sap.ui.core.BusyIndicator.show();
		},

		_hideBusy: function() {
			this.busyCount -= 1;
			if (this.busyCount <= 0) {
				this.busyCount = 0;
				sap.ui.core.BusyIndicator.hide();
			}
		},
		onEnabledMode: function(oZsftn, oType) {
			var t = this;
			var oChange = "",
				oCreate = "";
			switch (oType) {
				case "display":
					oChange = "";
					oCreate = "";
					this.byId("idEdit").setEnabled(true);
					this.byId("idDisplay").setEnabled(false);
					this.byId("idCreate").setEnabled(true);
					this.byId("idChangeButton").setEnabled(false);
					this.byId("idCreateButton").setEnabled(false);
					this.readData(oZsftn);
					break;
				case "change":
					this.byId("idEdit").setEnabled(false);
					this.byId("idDisplay").setEnabled(true);
					this.byId("idCreate").setEnabled(true);
					this.byId("idChangeButton").setEnabled(true);
					this.byId("idCreateButton").setEnabled(false);
					oChange = "change";
					oCreate = "";
					this.readData(oZsftn);
					break;
				case "create":
					oChange = "";
					oCreate = "create";
					this.byId("idEdit").setEnabled(true);
					this.byId("idDisplay").setEnabled(true);
					this.byId("idCreate").setEnabled(false);
					this.byId("idChangeButton").setEnabled(false);
					this.byId("idCreateButton").setEnabled(true);

					var oModel = new JSONModel();
					t.oView.setModel(oModel, "DisplayDocumentModel");
					break;
				default:
			}
			var oModel = new JSONModel(formatter.enabledModel(oChange, oCreate));
			this.oView.setModel(oModel, "enabledModel");
		},

		onEdit: function() {
			var t = this;
			this.onEnabledMode(this.oZsftn, "change");
		},

		onDisplay: function() {
			var t = this;
			this.onEnabledMode(this.oZsftn, "display");
		},

		onCreate: function() {
			var t = this;
			this.onEnabledMode(this.oZsftn, "create");
		},

		// onCreateButton: function() {
		// 	var t = this;
		// },

		onChangeButton: function() {
			var t = this;

			var model = this.oView.getModel("DisplayDocumentModel");
			var modelData = model.getData();
			this.sModel = JSON.parse(JSON.stringify(modelData));
			this.sModel.Erdat = new Date(this.sModel.Erdat);
			this.sModel.Aedat = new Date(this.sModel.Aedat);
			this.sModel.Fkdat = new Date(this.sModel.Fkdat);
			this.sModel.Bldat = new Date(this.sModel.Bldat);
			this.sModel.Budat = new Date(this.sModel.Budat);
			this.sModel.RectFkdat = new Date(this.sModel.RectFkdat);
			this.sModel.CapInvdate = new Date(this.sModel.CapInvdate);
			this.sModel.BldatCust = new Date(this.sModel.BldatCust);
			this.sModel.HuPymdate = new Date(this.sModel.HuPymdate);
			this.sModel.WadatIst = new Date(this.sModel.WadatIst);
			this.sModel.DeliveryStart = new Date(this.sModel.DeliveryStart);
			this.sModel.DeliveryEnd = new Date(this.sModel.DeliveryEnd);

			var sObjectPath = t.oView.getModel().createKey("/DocumentSet", {
				Zsftn: this.sModel.Zsftn
			});
			this.oView.getModel().update(sObjectPath, this.sModel, {
				success: function(oData, oResponse) {
					var oModel = new JSONModel(t.sModel);
					var oMessage = formatter._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);
					t.onDisplay();
					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});

		},

		onCreateButton: function() {
			var t = this;

			var model = this.oView.getModel("DisplayDocumentModel");
			var modelData = model.getData();
			var sModel = JSON.parse(JSON.stringify(modelData));
			/*			sModel.Erdat = new Date();
						sModel.Aedat = new Date();
						sModel.Fkdat = new Date();
						sModel.Bldat = new Date();
						sModel.Budat = new Date();
						sModel.RectFkdat = new Date();
						sModel.CapInvdate = new Date();
						sModel.BldatCust = new Date();
						sModel.HuPymdate = new Date();
						sModel.WadatIst = new Date();
						sModel.DeliveryStart = new Date();
						sModel.DeliveryEnd = new Date();*/

			this.oView.getModel().create("/DocumentSet", sModel, {
				success: function(oData, oResponse) {
					var oModel = new JSONModel(oData);
					var oMessage = formatter._jsonParse(oResponse.headers["sap-message"]);
					t.setPopupMessage(oMessage, oMessage.severity == "success" ? true : false);
					t._hideBusy();
				},
				error: function(err) {
					t._hideBusy();
				}
			});
		},
		
		handleMessagePopoverPress: function(oEvent) {
			if (!this.oMessagePopover) {
				this.createMessagePopover();
			}
			this.oMessagePopover.toggle(oEvent.getSource());
		},
		createMessagePopover: function(oEvent) {
			var that = this;
			var oLink = new Link({
				text: "Show more information",
				href: "http://google.com",
				target: "_blank"
			});

			var oMessageTemplate = new MessageItem({
				type: '{type}',
				title: '{title}',
				activeTitle: "{active}",
				description: '{description}',
				subtitle: '{subtitle}',
				counter: '{counter}',
				link: oLink
			});

			this.oMessagePopover = new MessagePopover({
				items: {
					path: '/',
					template: oMessageTemplate
				},
				activeTitlePress: function() {
					MessageToast.show('Active title is pressed');
				}
			});
		},

		setPopupMessage: function(data, oStatu) {
			var t = this;
			var sOdata = [];
			var sNew = [];
			if (!this.oMessagePopover) {
				this.createMessagePopover();
			}

			sNew = this.addMessage(data);
			var oModel = new JSONModel();
			oModel.setData(sNew);
			this.byId("idPopup").setText(sNew.length);
			this.oMessagePopover.setModel(oModel);
			if (!oStatu) {
				this.byId("idPopup").addDependent(this.oMessagePopover);
				this.oMessagePopover.openBy(this.byId("idPopup"));
			}
		},
		addMessage: function(line) {
			var t = this;
			var returnMessage = [];
			if (line.detail) {
				for (var i = 0; i < line.details.length; i++) {
					var sMessage = {
						type: line.details[i].code.split("/")[0],
						title: line.details[i].code.split("/")[0] + " Message",
						description: line.details[i].message,
						subtitle: line.details[i].code.split("/")[1],
						counter: 1
					};
					returnMessage.push(sMessage);
				}

			}
			var sMessage = {
				type: line.severity == "success" ? "Success" : "Error",
				title: line.code.split("/")[0] + " Message",
				description: line.message,
				subtitle: line.code.split("/")[1],
				counter: 1
			};
			returnMessage.push(sMessage);
			return returnMessage;
		},
		onValueHelpSelected: function(oEvent) {
			if (!this._valueHelpFragment) {
				this.valueHelpFragmentId = oEvent.mParameters.id.split("--")[2];
				this.valueHelpId = oEvent.mParameters.id.split("--")[3].split("id")[1];
				this._createValueHelp(this.valueHelpFragmentId, this.valueHelpId);
			} else {
				delete this._valueHelpFragment;
				this.valueHelpFragmentId = oEvent.mParameters.id.split("--")[2];
				this.valueHelpId = oEvent.mParameters.id.split("--")[3].split("id")[1];
				this._createValueHelp(this.valueHelpFragmentId, this.valueHelpId);
			}
			this._readVHData(this.valueHelpFragmentId, this.valueHelpId);
			this._valueHelpFragment.open();
		},
		_createValueHelp: function(fragmentId, HelpId) {
			if (!this._valueHelpFragment) {
				var fragmentId = "SNI.SII.view.fragments.ValueHelp." + HelpId;
				this._valueHelpFragment = sap.ui.xmlfragment(fragmentId, this);
				this._valueHelpFragment.setModel(this.getView().getModel("i18n"), "i18n");
			}
		},
		_readVHData: function(fragmentId, HelpId) {
			var t = this;
			this._showBusy();
			this.entitySetName = "/VH" + HelpId + "Set";
			this.collectionName = "/" + HelpId + "Collection";
			this.otherHelpId = [];
			this.oView.getModel().read(this.entitySetName, {
				success: function(oData, oResponse) {
					var oModel = new sap.ui.model.json.JSONModel({});
					oModel.setProperty(t.collectionName, oData.results);
					t._valueHelpFragment.setModel(oModel);
					for (var id in oData.results[0]) {
						if (id != "__metadata" && id != HelpId) {
							t.otherHelpId.push(id);
						}
					}

					t._hideBusy();
				},
				error: function(error) {
					t._hideBusy();
				},
			});
		},

		onSuggest: function(oEvent) {
			this.valueHelpFragmentId = oEvent.mParameters.id.split("--")[2];
			this.valueHelpId = oEvent.mParameters.id.split("--")[3].split("id")[1];
			var sTerm = oEvent.getParameter("suggestValue");
			var aFilters = [];
			if (sTerm) {
				aFilters.push(new Filter(this.valueHelpId, sap.ui.model.FilterOperator.StartsWith, sTerm));
			}
			oEvent.getSource().getBinding("suggestionItems").filter(aFilters);

		},

		onSuggestItemSelected: function(e) {
			var I = e.getParameter("selectedItem");
			var r = null;
			for (var i in I.getCustomData()) {
				var c = I.getCustomData()[i];
				if (c.getKey() === "")
					r = c.getValue();
			}
			this._setValueHelpData(I.getText(), r);
		},
		_setValueHelpData: function(r, a) {
			var fragmentId = this.getView().createId(this.valueHelpFragmentId);
			var sId = "id" + this.valueHelpId;
			var b = sap.ui.core.Fragment.byId(fragmentId, sId);
			if (b)
				b.setValue(r);
		},
		onInputFieldChanged: function(e) {
			var r = e.getSource();
			this.collectionName = "/" + HelpId + "Collection";
			this._setValueHelpData("", r.getValue());
			var c = function() {
				var m = this._valueHelpFragment.getModel();
				var R = m.getProperty(this.collectionName);
				for (var i = 0; i < R.length; i++) {
					var o = R[i];
					if (o[1].toUpperCase() === r.getValue().toUpperCase()) {
						this._setValueHelpData(o[S[this.valueHelpId]], r.getValue());
					}
				}
			};

			if (!this._valueHelpFragment) {
				if (oEvent.mParameters.id.split("--")[2] !== this.valueHelpFragmentId) {
					delete this._valueHelpFragment;
				}
				this._createValueHelp(this.valueHelpFragmentId, this.valueHelpId);
				this._readVHData(this.valueHelpFragmentId, this.valueHelpId);
			} else {
				c.call(this);
			}
		},
		onValueHelpSearch: function(e) {
			var value = e.getParameter("value");
			var filters = [];
			var oF1 = new sap.ui.model.Filter(this.valueHelpId, sap.ui.model.FilterOperator.Contains, value);
			filters.push(oF1)
			for (var i = 0; i < this.otherHelpId.length; i++) {
				var oF2 = new sap.ui.model.Filter(this.otherHelpId[i], sap.ui.model.FilterOperator.Contains, value);
				filters.push(oF2)

			}
			var oFilters = new sap.ui.model.Filter({
				filters: filters,
				and: false
			});
			e.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
		},
		onValueHelpConfirm: function(e) {
			var s = e.getParameter("selectedItem");
			if (s) {
				var S = s.getBindingContext().getObject();
				this._setValueHelpData(S[this.valueHelpId], S[1]);
			}
			e.getSource().getBinding("items").filter([]);
		},

		onValueHelpCancel: function(e) {
			e.getSource().getBinding("items").filter([]);
		},
		// ************************************************************************************
		
		onAdd: function(oEvent){
			
			
			this.oItemDialog = this.byId("idItemDialog");
			this.oItemDialog.open();
			
		},
		
		onItemSave: function(){
			this.oItemDialog.close();
			
			var oTable = this.byId(sap.ui.core.Fragment.createId("idItemFragment", "idTable"));
			var length = oTable.getRows().length;
			length++;
			oTable.setVisibleRowCount(length);
			var item = this.oView.getModel("DetailItemModel").getProperty("/ItemSet");
			
			item[length-1] = {"Zsftn":"000000001"+length};
			this.oView.getModel("DetailItemModel").setProperty("/ItemSet",item); 
		},
		
		onItemCancel: function(){
			this.oItemDialog.close();
		},
		
		onRemove: function(){
			var oTable = this.byId(sap.ui.core.Fragment.createId("idItemFragment", "idTable"));
			var selRows = oTable.getSelectedIndices();
			var length = oTable.getRows().length - selRows.length;
			oTable.setVisibleRowCount(length);
			
			var item = this.oView.getModel("DetailItemModel").getProperty("/ItemSet");
			
			for(var i=selRows.length-1;i>=0;i--){
				item.splice(selRows[i],1);
			}
			this.oView.getModel("DetailItemModel").setProperty("/ItemSet",item);
			oTable.clearSelection();
			
		}
		
		
	});
});