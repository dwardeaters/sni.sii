sap.ui.define([
		"SNI/SII/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("SNI.SII.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);